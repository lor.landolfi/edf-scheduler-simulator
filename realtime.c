#include "Tasks.h"



int main(void) {
	int i,j;
	printf("ciao");	
	// I open the file that will be used to read the input parameters
	FILE *fd;
	fd = fopen("parametri.txt", "r");
	if( fd==NULL ) {
		perror("Errore in apertura del file");
		exit(1);
	}

	//the first parameter read is the number of periodic tasks
	fscanf(fd, "%d", &NT);
	NT=NT+1;


	fscanf(fd, "%d", &tick);

	// I instantiate the array of task parameters, of threads and of pthread attributes
	tp = (struct task_par*)malloc((NT+9)*sizeof(struct task_par));
	tid = (pthread_t*)malloc((NT+9)*sizeof(pthread_t));
	att = (pthread_attr_t*)malloc((NT)*sizeof(pthread_attr_t));

	for(i=0; i<9; i++){
		indexS[i]=0;
		indexE[i]=0;
		for (j=0; j<9; j++){

		}
	}


	// the pthread "Activator" will be used as total bandwidth server
	pthread_t Activator;
	pthread_t Stop;
	pthread_t Notifier;



	// I in initialize the semaphores needed to wake up the sporadic tasks to 0

	for(i=0; i<9;i++){
		sem_init(&sem_to_At[i],0,0);
	}

	// now I initialize the semaphores needed to the task to communicate with the scheduler
	sem_init(&sem_to_scheduler, 0, 0);
	sem_init(&sem_from_scheduler, 0, 0);

	// the next semaphore is needed to access to the shared display in mutual exclusion
	sem_init(&sem_drow, 0, 1);

	// the next semaphore is used by the server to wait for the completion of the previously invoked sporadic task
	sem_init(&sem_from_At,0,0);
	sem_init(&semN, 0,0);

	// the barrier is used to synchronize all the thread at the beginning of their execution
	pthread_barrier_init(&barr, NULL, NT+10);


	// tempo is the variable representing the time i discrete units
	tempo=0;

	// routine to initialize allegro library
	al_init();
	al_install_keyboard();
	al_set_new_display_option(ALLEGRO_VSYNC,2,ALLEGRO_REQUIRE);
	al_init_primitives_addon();
	al_set_new_display_flags(ALLEGRO_WINDOWED);
	display = al_create_display(1280,800);
	al_clear_to_color(al_map_rgb(0, 0, 0));
	ALLEGRO_EVENT_QUEUE *event_queue = al_create_event_queue();
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	struct timespec t_curr,t_curr2;
	ALLEGRO_BITMAP * clearbitmap;
	clearbitmap=al_create_bitmap(1280,69);
	al_set_target_bitmap(clearbitmap);
	al_clear_to_color(al_map_rgb(0, 0, 0));
	ALLEGRO_BITMAP * bitmap;
	bitmap=al_create_bitmap(1,20);
	al_set_target_bitmap(bitmap);
	al_clear_to_color(al_map_rgb(100,120,1));
	al_set_target_bitmap(al_get_backbuffer(display));
	al_flip_display();

	//the main process writes the lines of time
	for (i = 0; i<=NT; i++){
		al_draw_line(0,(i+1)*70,1280,(i+1)*70,al_map_rgb(254,218,18),1);
		int stanghetta = 0;
		while (stanghetta <= 1280){
			al_draw_line(stanghetta,(i+1)*70,stanghetta,(i+1)*70+3,al_map_rgb(254,218,18),1);
			stanghetta = stanghetta + 20;
		}
	}



	al_init_font_addon(); // initialize the font addon
	ALLEGRO_FONT *f1;


	f1 = al_create_builtin_font();
	if (!f1) {

		return 1;
	}

	al_draw_textf(f1, al_map_rgb(255, 255, 0), 10, 10, 0,
			"Idle Time");

	//al_draw_textf(f1, al_map_rgb(255, 255, 0), 1004,15, 0,
	//	"400 milliseconds");
	al_draw_line(981,20,1000,20,al_map_rgb(254,218,18),1);
	al_draw_line(981,23,980,17,al_map_rgb(254,218,18),1);
	al_draw_line(1000,23,1000,17,al_map_rgb(254,218,18),1);

	char s[20];
	sprintf(s, " %d milliseconds", tick*20);
	al_draw_textf(f1, al_map_rgb(255, 255, 0), 1004,15, 0, s);









	// this is the priority that the scheduler thread will have
	tp[0].priority = 97;

	//I set the task parameters according to the input file
	for (i=0; i<NT; i++) {
		tp[i].arg = i;
		if (i > 0){
			fscanf(fd, "%d", &tp[i].period );
			tp[i].period = tp[i].period*tick;
			fscanf(fd, "%d",&tp[i].deadline);
			tp[i].deadline = tp[i].deadline*tick;

			//at the beginning, the priority are assigned according to the relative deadlines
			tp[i].priority = 30-(tp[i].deadline/1000);

			fscanf(fd, "%d",&tp[i].iterations);}
		tp[i].dmiss = 0;
		tp[i].display2=display;
		pthread_attr_init(&att[i]);
		pthread_attr_setinheritsched(&att[i], PTHREAD_EXPLICIT_SCHED);
		pthread_attr_setschedpolicy(&att[i], SCHED_FIFO);
		mypar.sched_priority = tp[i].priority;
		pthread_attr_setschedparam(&att[i], &mypar);
		if (i==0) pthread_create(&tid[0], &att[1],Scheduler,NULL);
		else pthread_create(&tid[i], &att[i], task, &tp[i]);}

	// the timeadder thread is the one that simply increment "tempo" every tick milliseconds
	pthread_create(&timeadder,&att[NT],Vuoto,NULL);
	pthread_setschedprio(timeadder,99);
	pthread_create(&Stop,NULL,Stopper, NULL);
	pthread_setschedprio(Stop,90);
	pthread_create(&Activator,&att[1],Attivatore,NULL);
	pthread_setschedprio(Activator,50);

	// The 9 pthreads implementing the sporadic tasks are created
	for(i=NT; i<NT+10; i++){
		tp[i].arg = i;
		pthread_create(&tid[i], &att[NT-1], Aperiodic_task, &tp[i]);
		pthread_setschedprio(tid[i], 1);
	}


	// The main waits for the pression of the esc key, when this happens, it destroys the display and terminates the program
	int mytime = 0;
	while(1){

		clock_gettime(CLOCK_MONOTONIC, &t_curr);

		sem_wait(&sem_drow);
		if (mytime > tempo) {
			al_draw_bitmap(clearbitmap,0,0,0);
			al_draw_textf(f1, al_map_rgb(255, 255, 0), 10, 10, 0,
					"Idle Time");
			sprintf(s, " %d milliseconds", tick*20);
			al_draw_textf(f1, al_map_rgb(255, 255, 0), 1004,15, 0, s);
			al_draw_line(981,20,1000,20,al_map_rgb(254,218,18),1);
			al_draw_line(981,23,980,17,al_map_rgb(254,218,18),1);
			al_draw_line(1000,23,1000,17,al_map_rgb(254,218,18),1);
		}
		al_draw_bitmap(bitmap,tempo,(1*70)-21,0);
		al_flip_display();
		sem_post(&sem_drow);

		time_add_ms(&t_curr,tick);
		clock_gettime(CLOCK_MONOTONIC, &t_curr2);
		while (time_cmp(t_curr2,t_curr)==-1){
			clock_gettime(CLOCK_MONOTONIC, &t_curr2);
		}
		mytime = tempo;
	}


}
