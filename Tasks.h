#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>
#include "allegro5/allegro_font.h"
#include <sched.h>
#include <semaphore.h>
#include <math.h>
#include <time.h>

int tick;
int NT;
ALLEGRO_DISPLAY *display;
int redraw;

struct task_par{
	int arg;
	int period;
	int deadline;
	int priority;
	int dmiss;
	struct timespec at;
	struct timespec dl;
	ALLEGRO_DISPLAY *display2;
	int iterations;
};

struct coef_queue{
	int colcoef1;
	int colcoef2;
	int colcoef3;
	struct timespec deadline;
};

struct coef_queue myColors[9][10];
int indexS[10];
int indexE[10];



int tempo;


struct task_par *tp;

struct sched_param mypar;

int *last;

pthread_attr_t *att;

pthread_t *tid;
pthread_t timeadder;

sem_t sem_drow;
sem_t sem_to_scheduler;
sem_t sem_from_scheduler;
sem_t sem_to_At[9];
sem_t sem_from_At;
sem_t semN;

struct timespec timeunit;

struct  timespec  tsSubtract(struct timespec t1, struct timespec t2);

void *Scheduler();

void* Vuoto();

void *Attivatore();

void *Aperiodic_task(void *arg);

void time_add_ms(struct timespec* t, int ms);

void time_add_ms(struct timespec* t, int ms);

void set_period(struct task_par *tp);

int deadline_miss(struct task_par *tp);

void wait_for_period(struct task_par *mytp);

struct timespec calculate_deadline(int id,struct timespec begin,double coef);

void *task(void *arg);

void time_copy(struct timespec *td, struct timespec ts);

int time_cmp(struct timespec t1, struct timespec t2);

pthread_barrier_t barr;
pthread_barrier_t barr2;

void sort(struct task_par array[], int begin, int end);

int FindMax(struct task_par Priorities[], int l, int mypr);

int FindMinAT(struct task_par *tp);

void swap(struct task_par *a, struct task_par *b);

void *Stopper();

void * Notificatore ();

struct timespec createApInstance(int Id, struct timespec TimetoAdd, double Server_coef, struct timespec CurrentTime );

void draw(int a);

struct coef_queue * pick (struct coef_queue *queue);

void add (struct coef_queue *queue, struct coef_queue *el);

void printCol();




